import vue from 'vue';
import validator from 'vue-validator';
import http from 'vue-resource';
import app from './components/app.vue';


vue.use(validator);

vue.validator('email', function (val) {
  return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(val)
});

vue.use(http);

new vue({
	el: 'body',
	components: {
		'subscribe': app
	}
})
